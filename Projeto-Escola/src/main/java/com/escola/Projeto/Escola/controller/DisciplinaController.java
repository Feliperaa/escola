package com.escola.Projeto.Escola.controller;

import com.escola.Projeto.Escola.model.Disciplina;
import com.escola.Projeto.Escola.service.DisciplinaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/disciplina")

public class DisciplinaController {
    @Autowired
    private DisciplinaService disciplinaService;

    @GetMapping
    public List<Disciplina> listarDisciplinas(){
        return disciplinaService.listarDisciplinas();
    }

    @PostMapping
    public ResponseEntity<Disciplina> addDisciplina(@RequestBody Disciplina disciplina){
        Disciplina auxDisciplina = disciplinaService.addDisciplina(disciplina);
        return ResponseEntity.status(HttpStatus.CREATED).body(auxDisciplina);
    }

    @PutMapping("/alterar/{id}")
    public ResponseEntity<Disciplina> alterarDisciplina (@RequestBody Disciplina disciplina, @PathVariable Long id){
        Disciplina auxDisciplina = disciplinaService.alterarDisciplina(disciplina, id);
        return ResponseEntity.status(HttpStatus.ACCEPTED).body(auxDisciplina);
    }




    @DeleteMapping("/delete/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deletarDisciplina(@PathVariable Long id){
        disciplinaService.deletarDisciplina(id);
    }

}
