package com.escola.Projeto.Escola.controller;

import com.escola.Projeto.Escola.model.Estudante;
import com.escola.Projeto.Escola.service.EstudanteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/estudante")

public class EstudandeController {
    @Autowired
    private EstudanteService estudanteService;

    @GetMapping
    public List<Estudante> listarEstudantes(){
        return estudanteService.listarEstudantes();
    }

   @PostMapping
    public ResponseEntity<Estudante> addEstudante (@RequestBody Estudante estudante){
        Estudante auxEstudante = estudanteService.addEstudante(estudante);
        return ResponseEntity.status(HttpStatus.CREATED).body(auxEstudante);
    }

    @PutMapping("/{id}")
    public ResponseEntity<Estudante> alterarEstudante (@RequestBody Estudante estudante, @PathVariable Long id){
        Estudante auxEstudante = estudanteService.alterarEstudante(estudante, id);
        return ResponseEntity.status(HttpStatus.ACCEPTED).body(auxEstudante);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deletarEstudante (@PathVariable Long id){
        estudanteService.deletarEstudante(id);
    }


}
