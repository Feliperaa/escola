package com.escola.Projeto.Escola.repository;

import com.escola.Projeto.Escola.model.Disciplina;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DisciplinaRepository extends JpaRepository <Disciplina, Long> {
}
