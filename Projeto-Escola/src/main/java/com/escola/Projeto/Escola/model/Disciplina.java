package com.escola.Projeto.Escola.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.ManyToAny;

import javax.persistence.*;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "disciplina")

public class Disciplina{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long codigo;
    private String nome;
    private Horario horario;
    @OneToMany
    @JoinColumn(name = "materias_id")
    private List<Turma> turma;

    @ManyToMany(cascade = CascadeType.PERSIST)
    @JoinTable(name = "disciplina_estudante",
               joinColumns = @JoinColumn(name = "disciplina_codigo"),
               inverseJoinColumns = @JoinColumn(name = "estudante_matricula"))
    private List<Estudante> estudantes;



}

