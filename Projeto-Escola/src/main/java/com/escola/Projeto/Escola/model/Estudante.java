package com.escola.Projeto.Escola.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Data
@Entity
@Table(name="estudante")
@NoArgsConstructor
@AllArgsConstructor

public class Estudante {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long matricula;
    private String nome;
    private String cpf;
    @OneToOne
    @JoinColumn(name = "endereco_id")
    private Endereco endereco;

    @ManyToMany(mappedBy = "estudantes")
    private List<Disciplina> disciplina;

}
