package com.escola.Projeto.Escola.service;

import com.escola.Projeto.Escola.model.Disciplina;
import com.escola.Projeto.Escola.repository.DisciplinaRepository;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class DisciplinaService {
    @Autowired
    private DisciplinaRepository disciplinaRepository;

    public List<Disciplina> listarDisciplinas() {
        return disciplinaRepository.findAll();
    }

    public Disciplina addDisciplina(Disciplina disciplina) {
        return disciplinaRepository.save(disciplina);
    }

    public void deletarDisciplina(Long id){
        disciplinaRepository.deleteById(id);
    }

    public Disciplina alterarDisciplina(Disciplina disciplina, Long id) {
        BeanUtils.copyProperties(disciplina, validarDisciplina(id),"codigo");
        return disciplinaRepository.save(validarDisciplina(id));
    }

    public Disciplina validarDisciplina(Long id){
        Optional<Disciplina> auxDisciplina = disciplinaRepository.findById(id);
        if (auxDisciplina.isEmpty()){
            throw new EmptyResultDataAccessException(1);
        }
        return auxDisciplina.get();
    }

}
