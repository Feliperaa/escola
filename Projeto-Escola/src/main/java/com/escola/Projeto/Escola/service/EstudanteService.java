package com.escola.Projeto.Escola.service;

import com.escola.Projeto.Escola.model.Estudante;
import com.escola.Projeto.Escola.repository.EstudanteRepository;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service

public class EstudanteService {
    @Autowired
    EstudanteRepository estudanteRepository;

    public List<Estudante> listarEstudantes() {
        return estudanteRepository.findAll();
    }


    public Estudante addEstudante(Estudante estudante) {
        return estudanteRepository.save(estudante);
    }


    public void deletarEstudante(Long id) {
        estudanteRepository.deleteById(id);
    }

    public Estudante alterarEstudante(Estudante estudante, Long id) {
        Estudante auxEstudante = validarEstudante(id);
        BeanUtils.copyProperties(estudante, auxEstudante,"matricula");
        return estudanteRepository.save(auxEstudante);
    }

    private Estudante validarEstudante (Long id){
        Optional<Estudante> auxEstudante = estudanteRepository.findById(id);
        if(auxEstudante.isEmpty()){
            throw new EmptyResultDataAccessException(1);
        }
        return auxEstudante.get();
    }

}
