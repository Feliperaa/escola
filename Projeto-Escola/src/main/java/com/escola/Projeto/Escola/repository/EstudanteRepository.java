package com.escola.Projeto.Escola.repository;

import com.escola.Projeto.Escola.model.Estudante;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;


public interface EstudanteRepository extends JpaRepository<Estudante, Long>{
}
